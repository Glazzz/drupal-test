<?php
/**
 * @file
 * chapter.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function chapter_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-chapters-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'chapters-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'books',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'chapters',
    'visibility' => 1,
  );

  return $export;
}
