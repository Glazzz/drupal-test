<?php
/**
 * @file
 * chapter.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function chapter_default_rules_configuration() {
  $items = array();
  $items['rules_director_role'] = entity_import('rules_config', '{ "rules_director_role" : {
      "LABEL" : "Director role",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "module" : { "label" : "Module", "type" : "text" },
        "delta" : { "label" : "Delta", "type" : "text" },
        "result" : { "label" : "Result", "type" : "boolean", "parameter" : false }
      },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "5" : "5" } }
          }
        }
      ],
      "DO" : [ { "data_set" : { "data" : [ "result" ], "value" : "1" } } ],
      "PROVIDES VARIABLES" : [ "result" ]
    }
  }');
  return $items;
}
