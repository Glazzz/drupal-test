<?php
/**
 * @file
 * chapter.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function chapter_user_default_roles() {
  $roles = array();

  // Exported role: Director.
  $roles['Director'] = array(
    'name' => 'Director',
    'weight' => 4,
  );

  return $roles;
}
