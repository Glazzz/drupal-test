<?php
/**
 * @file
 * character.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function character_default_rules_configuration() {
  $items = array();
  $items['rules_actor_role'] = entity_import('rules_config', '{ "rules_actor_role" : {
      "LABEL" : "Actor role",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "actor" ],
      "REQUIRES" : [ "rules" ],
      "ACCESS_EXPOSED" : "1",
      "USES VARIABLES" : {
        "module" : { "label" : "Module", "type" : "text" },
        "delta" : { "label" : "Delta", "type" : "text" },
        "result" : { "label" : "Result", "type" : "boolean", "parameter" : false }
      },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "4" : "4" } }
          }
        }
      ],
      "DO" : [ { "data_set" : { "data" : [ "result" ], "value" : "1" } } ],
      "PROVIDES VARIABLES" : [ "result" ]
    }
  }');
  return $items;
}
