<?php
/**
 * @file
 * character.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function character_user_default_roles() {
  $roles = array();

  // Exported role: Actor.
  $roles['Actor'] = array(
    'name' => 'Actor',
    'weight' => 3,
  );

  return $roles;
}
